all: panic

LDFLAGS += -lgpiodcxx -lgpiod

panic: panic.cpp
	${CXX} ${CXXFLAGS} ${LDFLAGS} $? -o $@

clean:
	rm -f panic
