/*
 * Emergency shutdown on power panic signal
 * Copyright 2019, Prusa Research s.r.o.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Prusa Research s.r.o.
 * Partyzánská 188/7a,
 * 17000 Praha 7,
 * Czech Republic
 *
 * info@prusa3d.com
 */

#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <gpiod.hpp>

using namespace std::chrono;
using namespace std::chrono_literals;

/*
 * This is a simple program using a gpiod library to wait for power panic signal.
 * Once the signal is detected the actionas are performed to minimize damage to
 * the system.
 */

// int main() {
// 	std::cout << "Initializing panic GPIO" << std::endl;
// 
// 	auto event_line = gpiod::find_line("panic");
// 	auto debug_line = gpiod::find_line("panic-debug");
// 	event_line.request({ "power-panic-react", gpiod::line_request::EVENT_FALLING_EDGE });
// 	if (debug_line) {
// 		debug_line.request({ "power-panic-react", gpiod::line_request::DIRECTION_OUTPUT}, 0);
// 	}
// 
// 	std::cout << "Waiting for panic" << std::endl;
// 	while (!event_line.event_wait(1s));
// 
//         Fire response actions
// 	if (debug_line) {
// 		debug_line.set_value(1);
// 	}
//         std::cout << "PANIC! PANIC! PANIC! Power is running out" << std::endl;
//         std::cout << "Trigering emergency system poweroff (o)" << std::endl;
//         std::ofstream sysrq ("/proc/sysrq-trigger");
//         sysrq << "o";
//         sysrq.close();
// 
//         Print something as long as possible
//         int cnt = 0;
//         while(true) {
//                 std::this_thread::sleep_for(10ms);
//                 cnt += 10;
//                 std::cout << "T+" << cnt << "ms" << std::endl;
//         }
// 
// 	return 0;
// }

/*
 * This is a NO ACTION version that just reports events
 */
int main() {
	std::cout << "Initializing panic GPIO" << std::endl;

	auto event_line = gpiod::find_line("panic");
	event_line.request({ "power-panic-react", gpiod::line_request::EVENT_BOTH_EDGES });

	steady_clock::time_point panic_begin = steady_clock::now();

	std::cout << "Waiting for power panic event" << std::endl;
	for(;;) {
		if (event_line.event_wait(1s)) {
			gpiod::line_event event = event_line.event_read();
			switch(event.event_type) {
				case gpiod::line_event::RISING_EDGE:
				{
					steady_clock::time_point panic_end = steady_clock::now();
					const auto time_ms = time_point_cast<milliseconds>(panic_end).time_since_epoch().count();
					const auto duration_us =  duration_cast<microseconds>(panic_end - panic_begin).count();
					std::cout << "Power recovered at: " << time_ms << "ms (duration: " << duration_us << "us)" << std::endl;
					break;
				}
				case gpiod::line_event::FALLING_EDGE:
				{
					panic_begin = steady_clock::now();
					const auto time_ms = time_point_cast<milliseconds>(panic_begin).time_since_epoch().count();
					std::cout << "Power lost at: " << time_ms << "ms" << std::endl;
					break;
				}
				default:
					std::cout << "Power panic - unknown event: " << event.event_type << std::endl;
			}
		}
	}

	return 0;
}
